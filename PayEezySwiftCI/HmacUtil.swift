//
//  HmacUtil.swift
//  PayEezySwiftCI
//
//  Created by Sean Kang on 4/8/20.
//  Copyright © 2020 JKangConsulting. All rights reserved.
//

import Foundation


class HmacUtil {
    
   

    
    
    static func secureRandomNumber() -> String {
        let number: Int = Int.random(in: 0 ..< 99999999999999999)
        return String(number)
    }
    
    
    static func generateHmac(body: String, nonce: String, timestamp: String) -> String{
        
        let  hmacInput = Constants.APIKEY+nonce+timestamp+Constants.MERCHANTTOKEN+body
        
        let crypto = EasyCrypt(secret: Constants.APISECRET, algorithm: .sha256)
        let result = crypto.hash(hmacInput)
        return result
    }
}


